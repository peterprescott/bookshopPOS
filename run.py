# Followed this simple tutorial:  https://scotch.io/tutorials/getting-started-with-flask-a-python-microframework
# run.py


from app import app

if __name__ == '__main__':
    app.run()
