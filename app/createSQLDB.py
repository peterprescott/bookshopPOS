import sqlite3
import json



conn = sqlite3.connect('books.db')

data = json.load(open('static/allTheBooks.json'))


c = conn.cursor()

# Create table
# ~ c.execute('''CREATE TABLE Books
             # ~ (Title, Author, Publisher, ISBN)''')



# Insert a row of data
for book in data:
	formattedBook = [( book["Title"] ,  book["Author"]  , book["Publisher"]  , book["ISBN"])]
	c.executemany("INSERT INTO Books VALUES (?,?,?,?)", formattedBook)
# ~ print(book['Title'])
# ~ print(formattedBook)




# ~ print(f"'{data[0]['Author']}'")

# ~ c.execute('DROP TABLE Books')

# Save (commit) the changes
conn.commit()

for row in c.execute('SELECT * FROM Books'):
	print(row)

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
